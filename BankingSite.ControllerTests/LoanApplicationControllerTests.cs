﻿using BankingSite.Controllers;
using BankingSite.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TestStack.FluentMVCTesting;

namespace BankingSite.ControllerTests
{
    [TestFixture]
    public class LoanApplicationControllerTests
    {

        [Test]

        public void  ShouldRenderDefaultView()
        {
            var fakeRepository = new Mock<IRepository>();
            var fakeLoanApplicationScorer = new Mock<ILoanApplicationScorer>();
            var sut = new LoanApplicationController(fakeRepository.Object, fakeLoanApplicationScorer.Object);
            sut.WithCallTo(x => x.Apply()).ShouldRenderDefaultView();


        }


        [Test]

        public void ShouldRedirectToAcceptedViewOnSuccessfulApplication()
        {
            var fakeRepository = new Mock<IRepository>();
            var fakeLoanApplicationScorer = new Mock<ILoanApplicationScorer>();
            var acceptedApplication = new LoanApplication
            {
                IsAccepted = true
            };
            var sut = new LoanApplicationController(fakeRepository.Object, fakeLoanApplicationScorer.Object);
            sut.WithCallTo(x => x.Apply(acceptedApplication)).ShouldRedirectTo<int>(x => x.Accepted);

        }


        [Test]

        public void ShouldRedirectToDeclingViewOnUnSuccessfulApplication()
        {
            var fakeRepository = new Mock<IRepository>();
            var fakeLoanApplicationScorer = new Mock<ILoanApplicationScorer>();
            var acceptedApplication = new LoanApplication
            {
                IsAccepted = false
            };
            var sut = new LoanApplicationController(fakeRepository.Object, fakeLoanApplicationScorer.Object);
            sut.WithCallTo(x => x.Apply(acceptedApplication)).ShouldRedirectTo<int>(x => x.Declined);

        }


    }
}
