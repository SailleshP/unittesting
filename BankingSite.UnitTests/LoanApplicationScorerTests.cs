﻿using BankingSite.Models;
using Moq;
using NUnit.Framework;

namespace BankingSite.UnitTests
{
    [TestFixture]
  public  class LoanApplicationScorerTests
    {
        [Test]

        public void ShouldDeclineWhenNotTooYoungAndWealthyButPoorCreditHistory_Classical()
        {

            //passing in real version of CreditHistoryChecker
            var suc = new LoanApplicationScorer(new CreditHistoryChecker());
            LoanApplication application = new LoanApplication()
            {
                Age = 22,
                //decline cheque
                FirstName = "sarah",
                LastName = "smith",
                AnnualIncome = 10000000.01m
            };

            suc.ScoreApplication(application);
            //We are asserting that the application that has been passed in has been declined even though
            //they pass basic test of age and minimum annual income
            Assert.That(application.IsAccepted, Is.False);


        }

        [Test]
        public void ShouldDeclineWhenNotTooYoungAndWealthyButPoorCreditHistory_MOQ()
        {
            var fakeCreditHistoryChecker = new Mock<ICreditHistoryChecker>();
            //return false to return back credit
            fakeCreditHistoryChecker
                                .Setup(x => x.CheckCreditHistory(It.IsAny<string>(), It.IsAny<string>()))
                                .Returns(false);
            

            //passing in real version of CreditHistoryChecker
            var suc = new LoanApplicationScorer(fakeCreditHistoryChecker.Object);
            LoanApplication application = new LoanApplication()
            {
                //this is logic inside our LoanApplicationScorer
                Age = 22,
                AnnualIncome = 10000000.01m
            };

            suc.ScoreApplication(application);
            //We are asserting that the application that has been passed in has been declined even though
            //they pass basic test of age and minimum annual income
            Assert.That(application.IsAccepted, Is.False);


        }
    }
}
