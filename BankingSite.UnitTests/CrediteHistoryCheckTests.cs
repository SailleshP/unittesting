﻿using BankingSite.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingSite.UnitTests
{
    [TestFixture]
   public class CrediteHistoryCheckTests
    {
        [Test]

        public void  ShouldRecognizePeopleWithBadCredit()
        {
            var sut = new CreditHistoryChecker();
            var isCreditWorthy = sut.CheckCreditHistory("sarah","smith");
            Assert.That(isCreditWorthy, Is.False);
        }

        [Test]

        public void ShouldOkPeopleWithGoodCredit()
        {
            var sut = new CreditHistoryChecker();
            var isCreditWorthy = sut.CheckCreditHistory("saillesh", "pawar");
            Assert.That(isCreditWorthy, Is.True);
        }


    }
}
