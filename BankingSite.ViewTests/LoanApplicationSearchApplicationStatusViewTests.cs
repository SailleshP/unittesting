﻿using BankingSite.Models;
using HtmlAgilityPack;
using NUnit.Framework;
using RazorGenerator.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingSite.ViewTests
{
    [TestFixture]
   public class LoanApplicationSearchApplicationStatusViewTests
    {

        [Test]
        public void ShouldRenderAcceptedMessages()
        {
            var sut = new Views.LoanApplicationSearch.ApplicationStatus();
            var model = new LoanApplication()
            {
                IsAccepted = true
            };


            HtmlDocument html = sut.RenderAsHtml(model);

            var isAcceptedMessageRendered = html.GetElementbyId("acceptedMessage") != null;
            var isDeclineMessageRendered = html.GetElementbyId("acceptedMessage") != null;


            Assert.That(isAcceptedMessageRendered, Is.True);
            Assert.That(isAcceptedMessageRendered, Is.False);

            //var actualMessage = html.GetElementbyId("acceptedMessage").InnerText;
            //var actualMessage = html.GetElementbyId("status").InnerText;
            //Assert.That(actualMessage, Is.EqualTo("Yay! Accepted!"));
            //Assert.That(actualMessage, Contains.Substring("Yay! Accepted!"));

        }

    }
}
